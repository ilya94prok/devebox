/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32h7xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "asyncEventTimer.h"
/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */
#define USE_SPI_IT_WITHOUT_DMA
#define US_DELAY_NRF_PACKET		460
#define US_DELAY_START_SHUTTER_TO_REAL_SHUTTER		10
#define US_DELAY_CAM_CALIB				4600
/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

void HAL_TIM_MspPostInit(TIM_HandleTypeDef *htim);

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define MPU_CS_Pin GPIO_PIN_10
#define MPU_CS_GPIO_Port GPIOE
#define MPU_INT_Pin GPIO_PIN_11
#define MPU_INT_GPIO_Port GPIOE
#define MPU_INT_EXTI_IRQn EXTI15_10_IRQn
#define EXPOS_LED_Pin GPIO_PIN_15
#define EXPOS_LED_GPIO_Port GPIOB
#define EXPOS_LED_EXTI_IRQn EXTI15_10_IRQn
#define RADIO_SYNC_Pin GPIO_PIN_8
#define RADIO_SYNC_GPIO_Port GPIOD
#define RADIO_SYNC_EXTI_IRQn EXTI9_5_IRQn
#define TERMO_STAB_Pin GPIO_PIN_12
#define TERMO_STAB_GPIO_Port GPIOD
#define NRF_INT_Pin GPIO_PIN_0
#define NRF_INT_GPIO_Port GPIOD
#define NRF_INT_EXTI_IRQn EXTI0_IRQn
#define NRF_CS_Pin GPIO_PIN_1
#define NRF_CS_GPIO_Port GPIOD
#define NRF_CE_Pin GPIO_PIN_2
#define NRF_CE_GPIO_Port GPIOD
#define MS5611_CS_Pin GPIO_PIN_5
#define MS5611_CS_GPIO_Port GPIOB
#define NOT_USED_PERIPH_CS_Pin GPIO_PIN_6
#define NOT_USED_PERIPH_CS_GPIO_Port GPIOB
/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
