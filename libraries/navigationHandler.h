#ifndef NAVIGATIONHANDLER_H_
#define NAVIGATIONHANDLER_H_

#include "nav_srcdkf.h"
#include "main.h"
#include "cmsis_os.h"

#include "mpu9250.h"
#include "findBlobs.h"

typedef struct{
	nav_srcdkf_t ukf;

	osThreadId theadID;

	mpu9250_t *imu;
	findBlobs_t *blobs;
	circularIMUBuf_t imuBuf[SIZE_BUFFER_IMU];

	float freqPredictThread;
	float freqPredictIMU;
	float freqUpdateBeaconThread;
	float freqUpdateBeacon;

	uint32_t latencyCopyImuBuf;
	uint32_t latencyPredict;
	uint32_t latencyZeroVel;
	uint32_t latencyUpdateBeacon;
	uint32_t latencyFullCycle;
	uint32_t latencyFromImuToResultEstimate;
	uint32_t latencyFromCaptureToResultEstimate;

	uint32_t usLastPredict, usLastBeacon;
}navigationHandler_t;

void NavigationHandlerInit(navigationHandler_t *nav, float *states, int hist, mpu9250_t *imu, findBlobs_t *blobs);

#endif /* NAVIGATIONHANDLER_H_ */
