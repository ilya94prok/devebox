#include "nav_srcdkf.h"
#include "string.h"

const float v0a[3] = { 0.0f, 0.0f, 1.0f };
const float vdir[3] = { 0.0f, 1.0f, 0.0f };


//private
static void navUkfInitState(nav_srcdkf_t *ukf, float *states)
{
	if (states == 0)
	{
		//vel
		ukf->x[UKF2_STATE_VELX] = 0;
		ukf->x[UKF2_STATE_VELY] = 0;
		ukf->x[UKF2_STATE_VELZ] = 0;

		// pos
		ukf->x[UKF2_STATE_POSX] = 0;
		ukf->x[UKF2_STATE_POSY] = 0;
		ukf->x[UKF2_STATE_POSZ] = 0;

		// acc bias
		ukf->x[UKF2_STATE_ACC_BIAS_X] = 0;
		ukf->x[UKF2_STATE_ACC_BIAS_Y] = 0;
		ukf->x[UKF2_STATE_ACC_BIAS_Z] = 0;

		// gyo bias
		ukf->x[UKF2_STATE_GYO_BIAS_X] = 0;
		ukf->x[UKF2_STATE_GYO_BIAS_Y] = 0;
		ukf->x[UKF2_STATE_GYO_BIAS_Z] = 0;

		// quat
		ukf->x[UKF2_STATE_Q1] = 1;
		ukf->x[UKF2_STATE_Q2] = 0;
		ukf->x[UKF2_STATE_Q3] = 0;
		ukf->x[UKF2_STATE_Q4] = 0;

		return;
	}

	for (int i = 0 ; i < SIM_S; i++)
	{
		ukf->x[i] = states[i];
	}
}
static uint8_t QuatInit(nav_srcdkf_t *ukf, vector_t accv, vector_t gyo, vector_t y)
{
	if (ukf->cntInitQuat > 100)
	{
		return 1;
	}
	else
	{
		float estAcc[3], estDir[3];

		float rotError[3];
		float acc[3], dir[3];
		float q3[4], q2[4];

		acc[0] = accv.x;
		acc[1] = accv.y;
		acc[2] = accv.z;

		dir[0] = y.x;
		dir[1] = y.y;
		dir[2] = y.z;

		// rotate gravity to body frame of reference
		rotateVectorByRevQuat(v0a, &ukf->x[UKF2_STATE_Q1], estAcc);

		normalizeVec(acc, acc);

		// measured error, starting with ACC vector
		rotError[0] = -(acc[2] * estAcc[1] - estAcc[2] * acc[1]) * 0.5f;
		rotError[1] = -(acc[0] * estAcc[2] - estAcc[0] * acc[2]) * 0.5f;
		rotError[2] = -(acc[1] * estAcc[0] - estAcc[1] * acc[0]) * 0.5f;

		rotateVectorByRevQuat(vdir, &ukf->x[UKF2_STATE_Q1], estDir);

		// measured error, starting with ACC vector
		if (dir[0] == 0 && dir[1] == 0 && dir[2] == 0)
		{

		}
		else
		{
			rotError[0] += -(dir[2] * estDir[1] - estDir[2] * dir[1]) * 0.5f;
			rotError[1] += -(dir[0] * estDir[2] - estDir[0] * dir[2]) * 0.5f;
			rotError[2] += -(dir[1] * estDir[0] - estDir[1] * dir[0]) * 0.5f;
		}

		exp_mapQuat(rotError, q3);
		quatMultiply(&ukf->x[UKF2_STATE_Q1], q3, q2);

		ukf->x[UKF2_STATE_Q1] = q2[0];
		ukf->x[UKF2_STATE_Q2] = q2[1];
		ukf->x[UKF2_STATE_Q3] = q2[2];
		ukf->x[UKF2_STATE_Q4] = q2[3];


		normalizeQuat(&ukf->x[UKF2_STATE_Q1], &ukf->x[UKF2_STATE_Q1]);

		ukf->cntInitQuat++;

		return 0;
	}


}

static void TimeUpdate(float *in, float *noise, float *out, float *u, float dt, int n) {
	float tmp[3], acc[3];
	float rate[3];
	float q[4], q2[4], q3[4];
	int i;
	float vel1[3], pos1[3];
	float sqrtDt = sqrtf(dt);
	// assume out == in
	out = in;
	float gravity[3];
	float tmpMat3x3[9];
	for (i = 0; i < n; i++) {

		rate[0] = (u[3] + in[UKF2_STATE_GYO_BIAS_X*n + i])*dt + noise[UKF2_V_NOISE_RATE_X*n + i] * sqrtDt;
		rate[1] = (u[4] + in[UKF2_STATE_GYO_BIAS_Y*n + i])*dt + noise[UKF2_V_NOISE_RATE_Y*n + i] * sqrtDt;
		rate[2] = (u[5] + in[UKF2_STATE_GYO_BIAS_Z*n + i])*dt + noise[UKF2_V_NOISE_RATE_Z*n + i] * sqrtDt;
		/**************************/
		// acc
		q[0] = in[UKF2_STATE_Q1*n + i];
		q[1] = in[UKF2_STATE_Q2*n + i];
		q[2] = in[UKF2_STATE_Q3*n + i];
		q[3] = in[UKF2_STATE_Q4*n + i];

		rotateVectorByRevQuat(v0a, q, gravity);

		acc[0] = u[0] + in[UKF2_STATE_ACC_BIAS_X*n + i] - gravity[0] * GRAVITY;
		acc[1] = u[1] + in[UKF2_STATE_ACC_BIAS_Y*n + i] - gravity[1] * GRAVITY;
		acc[2] = u[2] + in[UKF2_STATE_ACC_BIAS_Z*n + i] - gravity[2] * GRAVITY;
		/**************************/
		// pos
		//(identityMat3x3 - skewMat(rate)
		tmpMat3x3[0] = 1.0f;
		tmpMat3x3[1] = rate[2];
		tmpMat3x3[2] = -rate[1];
		tmpMat3x3[3] = -rate[2];
		tmpMat3x3[4] = 1.0f;
		tmpMat3x3[5] = rate[0];
		tmpMat3x3[6] = rate[1];
		tmpMat3x3[7] = -rate[0];
		tmpMat3x3[8] = 1.0f;

		tmp[0] = in[UKF2_STATE_POSX*n + i];
		tmp[1] = in[UKF2_STATE_POSY*n + i];
		tmp[2] = in[UKF2_STATE_POSZ*n + i];

		multiply3x3_3x1(pos1, tmpMat3x3, tmp);

		out[UKF2_STATE_POSX*n + i] = pos1[0] + in[UKF2_STATE_VELX*n + i] * dt;
		out[UKF2_STATE_POSY*n + i] = pos1[1] + in[UKF2_STATE_VELY*n + i] * dt;
		out[UKF2_STATE_POSZ*n + i] = pos1[2] + in[UKF2_STATE_VELZ*n + i] * dt;

		/**************************/
		//vel
		tmp[0] = in[UKF2_STATE_VELX*n + i];
		tmp[1] = in[UKF2_STATE_VELY*n + i];
		tmp[2] = in[UKF2_STATE_VELZ*n + i];

		multiply3x3_3x1(vel1, tmpMat3x3, tmp);

		out[UKF2_STATE_VELX*n + i] = vel1[0] + acc[0] * dt + noise[UKF2_V_NOISE_VEL_X*n + i] * sqrtDt;
		out[UKF2_STATE_VELY*n + i] = vel1[1] + acc[1] * dt + noise[UKF2_V_NOISE_VEL_Y*n + i] * sqrtDt;
		out[UKF2_STATE_VELZ*n + i] = vel1[2] + acc[2] * dt + noise[UKF2_V_NOISE_VEL_Z*n + i] * sqrtDt;

		exp_mapQuat(rate, q3);
		quatMultiply(q, q3, q2);
		out[UKF2_STATE_Q1*n + i] = q2[0];
		out[UKF2_STATE_Q2*n + i] = q2[1];
		out[UKF2_STATE_Q3*n + i] = q2[2];
		out[UKF2_STATE_Q4*n + i] = q2[3];

		// acc bias
		out[UKF2_STATE_ACC_BIAS_X*n + i] = in[UKF2_STATE_ACC_BIAS_X*n + i] + noise[UKF2_V_NOISE_ACC_BIAS_X*n + i] * sqrtDt;
		out[UKF2_STATE_ACC_BIAS_Y*n + i] = in[UKF2_STATE_ACC_BIAS_Y*n + i] + noise[UKF2_V_NOISE_ACC_BIAS_Y*n + i] * sqrtDt;
		out[UKF2_STATE_ACC_BIAS_Z*n + i] = in[UKF2_STATE_ACC_BIAS_Z*n + i] + noise[UKF2_V_NOISE_ACC_BIAS_Z*n + i] * sqrtDt;
		/**************************/
		// gbias
		out[UKF2_STATE_GYO_BIAS_X*n + i] = in[UKF2_STATE_GYO_BIAS_X*n + i] + noise[UKF2_V_NOISE_GYO_BIAS_X*n + i] * sqrtDt;
		out[UKF2_STATE_GYO_BIAS_Y*n + i] = in[UKF2_STATE_GYO_BIAS_Y*n + i] + noise[UKF2_V_NOISE_GYO_BIAS_Y*n + i] * sqrtDt;
		out[UKF2_STATE_GYO_BIAS_Z*n + i] = in[UKF2_STATE_GYO_BIAS_Z*n + i] + noise[UKF2_V_NOISE_GYO_BIAS_Z*n + i] * sqrtDt;
	}
}
static void navUkfRateUpdate(float *u, float *x, float *noise, float *y)
{
	y[0] = -x[UKF2_STATE_GYO_BIAS_X + (int)u[2]] + noise[0];
}
static void navUkfVelUpdate(float *u, float *x, float *noise, float *y)
{
	y[0] = x[UKF2_STATE_VELX] + noise[0]; // return velocity
	y[1] = x[UKF2_STATE_VELY] + noise[1];
	y[2] = x[UKF2_STATE_VELZ] + noise[2];

}
static void navUkfBeaconUpdateEUCM(float *u, float *x, float *noise, float *y)
{
	float rot[3], trans[3], rot2[3];

	//������� ����� � �� ���
	rotateVectorByRevQuat(&u[2], &x[UKF2_STATE_Q1], trans);
	//���� ������������ ������
	rot[0] = trans[0] - (x[UKF2_STATE_POSX] - u[14]);
	rot[1] = trans[1] - (x[UKF2_STATE_POSY] - u[15]);
	rot[2] = trans[2] - (x[UKF2_STATE_POSZ] - u[16]);

	//������� �� ��� � ������ ������� ������
	rotateVectorByRevQuat(rot, &u[10], rot2);

	distortEUCM(&y[0], &y[1], rot2, u[5], u[6], u[7], u[9], u[17], u[18]);

	y[0] += noise[0];
	y[1] += noise[1];
}

//public
void nav_srcdkfInit(nav_srcdkf_t *ukf, float *states, int hist)
{
	float Q[SIM_S];		// state variance
	float V[SIM_V];		// process variance

	ukf->sizeHist = hist;

	ukf->posN = (float32_t *)aqDataCalloc(hist, sizeof(float32_t));
	ukf->posE = (float32_t *)aqDataCalloc(hist, sizeof(float32_t));
	ukf->posD = (float32_t *)aqDataCalloc(hist, sizeof(float32_t));
	ukf->velN =(float32_t *)aqDataCalloc(hist, sizeof(float32_t));
	ukf->velE = (float32_t *)aqDataCalloc(hist, sizeof(float32_t));
	ukf->velD = (float32_t *)aqDataCalloc(hist, sizeof(float32_t));

	ukf->q0Hist = (float32_t *)aqDataCalloc(hist, sizeof(float32_t));
	ukf->q1Hist = (float32_t *)aqDataCalloc(hist, sizeof(float32_t));
	ukf->q2Hist = (float32_t *)aqDataCalloc(hist, sizeof(float32_t));
	ukf->q3Hist = (float32_t *)aqDataCalloc(hist, sizeof(float32_t));

	ukf->timeus = (uint32_t*)aqDataCalloc(hist, sizeof(uint32_t));

	ukf->navHistIndex = 0;
	ukf->cntInitQuat = 0;
	ukf->ptrIMUBuf = 0;
	ukf->initSDBuf = 0;
	ukf->motion = 1;

	srcdkfInit(&ukf->srcdkf, SIM_S, SIM_M, SIM_V, SIM_N);
	ukf->x = srcdkfGetState(&ukf->srcdkf);


	Q[0] = UKF2_VEL_Q;
	Q[1] = UKF2_VEL_Q;
	Q[2] = UKF2_VEL_ALT_Q;
	Q[3] = UKF2_POS_Q;
	Q[4] = UKF2_POS_Q;
	Q[5] = UKF2_POS_Q;
	Q[6] = UKF2_ACC_BIAS_Q;
	Q[7] = UKF2_ACC_BIAS_Q;
	Q[8] = UKF2_ACC_BIAS_Q;
	Q[9] = UKF2_GYO_BIAS_Q;
	Q[10] = UKF2_GYO_BIAS_Q;
	Q[11] = UKF2_GYO_BIAS_Q;
	Q[12] = UKF2_QUAT_Q;
	Q[13] = UKF2_QUAT_Q;
	Q[14] = UKF2_QUAT_Q;
	Q[15] = UKF2_QUAT_Q;



	V[UKF2_V_NOISE_ACC_BIAS_X] = UKF2_ACC_BIAS_V;
	V[UKF2_V_NOISE_ACC_BIAS_Y] = UKF2_ACC_BIAS_V;
	V[UKF2_V_NOISE_ACC_BIAS_Z] = UKF2_ACC_BIAS_V;

	V[UKF2_V_NOISE_GYO_BIAS_X] = UKF2_GYO_BIAS_V;
	V[UKF2_V_NOISE_GYO_BIAS_Y] = UKF2_GYO_BIAS_V;
	V[UKF2_V_NOISE_GYO_BIAS_Z] = UKF2_GYO_BIAS_V;

	V[UKF2_V_NOISE_RATE_X] = UKF2_RATE_V;
	V[UKF2_V_NOISE_RATE_Y] = UKF2_RATE_V;
	V[UKF2_V_NOISE_RATE_Z] = UKF2_RATE_V;

	V[UKF2_V_NOISE_VEL_X] = UKF2_VEL_V;
	V[UKF2_V_NOISE_VEL_Y] = UKF2_VEL_V;
	V[UKF2_V_NOISE_VEL_Z] = UKF2_VEL_V;


	srcdkfSetVariance(&ukf->srcdkf, Q, V, 0, 0);
	navUkfInitState(ukf, states);
}

void nav_srcdkfZeroVel(nav_srcdkf_t *ukf)
{
	float y[3];
	float noise[3];
	uint32_t u[5 + 3 + 1 + 1];
	u[0] = 0;
	u[1] = 0;

	u[2] = 0.0f;
	u[3] = 0.0f;
	u[4] = 0.0f;
	u[5] = 0.0f;
	u[6] = 0.0f;
	u[7] = 0.0f;
	u[8] = 0;

	y[0] = 0.0f;
	y[1] = 0.0f;
	y[2] = 0.0f;


	noise[0] = 1e-7f;
	noise[1] = 1e-7f;
	noise[2] = 1e-7f;


	srcdkfMeasurementUpdate(&ukf->srcdkf, (float*)u, y, 3, 3, noise, navUkfVelUpdate);
}
void nav_srcdkfZeroRate(nav_srcdkf_t *ukf, float rate, int axis)
{
	float noise[1];        // measurement variance
	float y[1];            // measurment(s)
	float u[5 + 3 + 1 + 1];		   // user data
	*((uint32_t*)u + 0) = 0;
	*((uint32_t*)u + 1) = 0;
	u[8] = 0;

	noise[0] = 0.0000001f;
	y[0] = rate;
	u[2] = (float)axis;

	srcdkfMeasurementUpdate(&ukf->srcdkf, u, y, 1, 1, noise, navUkfRateUpdate);
}

void nav_srcdkfBeaconUpdateEUCM(nav_srcdkf_t *ukf, vector_t posBeacon, float pixX, float pixY, float noise, float mahalOfsset, float* realMahal, uint32_t time)
{
	float y[2], n[2];
	float u[5 + 3 + 1 + 1 + 3 + 4 + 2];
	float posDelta[3];
	float velDelta[3];
	float qDelta[3], qcur[4], qRes[4];
	*((uint32_t*)u + 0) = 0;
	*((uint32_t*)u + 1) = 0;
	//*((uint32_t*)u + 8) = 0;



	u[2] = posBeacon.x;
	u[3] = posBeacon.y;
	u[4] = posBeacon.z;
	u[5] = ukf->fx;
	u[6] = ukf->fy;
	u[7] = ukf->cx;
	u[8] = mahalOfsset;
	u[9] = ukf->cy;
	u[10] = ukf->qCamToImu[0];
	u[11] = ukf->qCamToImu[1];
	u[12] = ukf->qCamToImu[2];
	u[13] = ukf->qCamToImu[3];
	u[14] = ukf->posCamToImu[0];
	u[15] = ukf->posCamToImu[1];
	u[16] = ukf->posCamToImu[2];
	u[17] = ukf->alpha;
	u[18] = ukf->beta;

	n[0] = noise;
	n[1] = noise;

	y[0] = pixX;
	y[1] = pixY;

	// determine how far back this GPS position update came from
	int32_t histIndex = ukf->navHistIndex;
	int32_t minTime = 9999999, minIndex = 0, stepsBack = 0;
	while (1)
	{
		int timeDt = abs((int64_t)ukf->timeus[histIndex] - (int64_t)time);
		if (timeDt < 7000)
			break;
		if (timeDt < minTime)
		{
			minTime = timeDt;
			minIndex = histIndex;
		}
		histIndex = (histIndex - 1 < 0) ? ukf->sizeHist : histIndex - 1;
		stepsBack++;
		if (histIndex == ukf->navHistIndex)
		{
			histIndex = minIndex;
			break;
		}
	}
	ukf->stepsBack = stepsBack;

	// calculate delta from current position
	posDelta[0] = ukf->x[UKF2_STATE_POSX] - ukf->posN[histIndex];
	posDelta[1] = ukf->x[UKF2_STATE_POSY] - ukf->posE[histIndex];
	posDelta[2] = ukf->x[UKF2_STATE_POSZ] - ukf->posD[histIndex];
	// calculate delta from current position
	velDelta[0] = ukf->x[UKF2_STATE_VELX] - ukf->velN[histIndex];
	velDelta[1] = ukf->x[UKF2_STATE_VELY] - ukf->velE[histIndex];
	velDelta[2] = ukf->x[UKF2_STATE_VELZ] - ukf->velD[histIndex];

	qcur[0] = ukf->q0Hist[histIndex];
	qcur[1] = ukf->q1Hist[histIndex];
	qcur[2] = ukf->q2Hist[histIndex];
	qcur[3] = ukf->q3Hist[histIndex];


	box_minusQuat(&ukf->x[UKF2_STATE_Q1], qcur, qDelta);

	ukf->x[UKF2_STATE_Q1] =  ukf->q0Hist[histIndex];
	ukf->x[UKF2_STATE_Q2] =  ukf->q1Hist[histIndex];
	ukf->x[UKF2_STATE_Q3] =  ukf->q2Hist[histIndex];
	ukf->x[UKF2_STATE_Q4] =  ukf->q3Hist[histIndex];

	// set current position state to historic data
	ukf->x[UKF2_STATE_POSX] = ukf->posN[histIndex];
	ukf->x[UKF2_STATE_POSY] = ukf->posE[histIndex];
	ukf->x[UKF2_STATE_POSZ] = ukf->posD[histIndex];
	// set current position state to historic data
	ukf->x[UKF2_STATE_VELX] = ukf->velN[histIndex];
	ukf->x[UKF2_STATE_VELY] = ukf->velE[histIndex];
	ukf->x[UKF2_STATE_VELZ] = ukf->velD[histIndex];

	srcdkfMeasurementUpdate(&ukf->srcdkf, (float*)u, y, 2, 2, n, navUkfBeaconUpdateEUCM);

	box_plusQuat(&ukf->x[UKF2_STATE_Q1], qDelta, qRes);

	ukf->x[UKF2_STATE_Q1] = qRes[0];
	ukf->x[UKF2_STATE_Q2] = qRes[1];
	ukf->x[UKF2_STATE_Q3] = qRes[2];
	ukf->x[UKF2_STATE_Q4] = qRes[3];

	// add the historic position delta back to the current state
	ukf->x[UKF2_STATE_POSX] += posDelta[0];
	ukf->x[UKF2_STATE_POSY] += posDelta[1];
	ukf->x[UKF2_STATE_POSZ] += posDelta[2];
	// add the historic position delta back to the current state
	ukf->x[UKF2_STATE_VELX] += velDelta[0];
	ukf->x[UKF2_STATE_VELY] += velDelta[1];
	ukf->x[UKF2_STATE_VELZ] += velDelta[2];

	*realMahal = u[9];
}

void nav_srcdkfInertialUpdate(nav_srcdkf_t *ukf, vector_t acc, vector_t gyo, vector_t y, float dt, uint32_t time)
{

	float u[6];
	float magAcc, magGyo;
	float posGlob[3], velGlob[3];
	u[0] = acc.x;
	u[1] = acc.y;
	u[2] = acc.z;

	u[3] = gyo.x;
	u[4] = gyo.y;
	u[5] = gyo.z;

	magGyo = lengthVector(&u[3]);
	magAcc = lengthVector(&u[0]);

	ukf->accBuf[ukf->ptrIMUBuf] = magAcc;
	ukf->gyoBuf[ukf->ptrIMUBuf] = magGyo;

	ukf->ptrIMUBuf = (ukf->ptrIMUBuf + 1) % 64;
	if (ukf->ptrIMUBuf == 0)
		ukf->initSDBuf = 1;

	if (ukf->initSDBuf)
	{
		arm_std_f32(ukf->accBuf, 64, &ukf->SDAcc);
		arm_std_f32(ukf->gyoBuf, 64, &ukf->SDGyo);

		if (ukf->SDAcc > IMU_ACC_STATIC_STD || ukf->SDGyo > IMU_GYO_STATIC_STD)
			ukf->motion = 1;
		else
			ukf->motion = 0;
	}



	if (QuatInit(ukf, acc, gyo, y))
	{
		srcdkfTimeUpdate(&ukf->srcdkf, u, dt, TimeUpdate);
	}
	normalizeQuat(&ukf->x[UKF2_STATE_Q1], &ukf->x[UKF2_STATE_Q1]);
	// store history
	ukf->posN[ukf->navHistIndex] = ukf->x[UKF2_STATE_POSX];
	ukf->posE[ukf->navHistIndex] = ukf->x[UKF2_STATE_POSY];
	ukf->posD[ukf->navHistIndex] = ukf->x[UKF2_STATE_POSZ];

	ukf->velN[ukf->navHistIndex] = ukf->x[UKF2_STATE_VELX];
	ukf->velE[ukf->navHistIndex] = ukf->x[UKF2_STATE_VELY];
	ukf->velD[ukf->navHistIndex] = ukf->x[UKF2_STATE_VELZ];

	ukf->q0Hist[ukf->navHistIndex] = ukf->x[UKF2_STATE_Q1];
	ukf->q1Hist[ukf->navHistIndex] = ukf->x[UKF2_STATE_Q2];
	ukf->q2Hist[ukf->navHistIndex] = ukf->x[UKF2_STATE_Q3];
	ukf->q3Hist[ukf->navHistIndex] = ukf->x[UKF2_STATE_Q4];

	ukf->timeus[ukf->navHistIndex] = time;

	ukf->navHistIndex = (ukf->navHistIndex + 1) % ukf->sizeHist;


	rotateVectorByQuat(&ukf->x[UKF2_STATE_POSX], &ukf->x[UKF2_STATE_Q1], posGlob);
	rotateVectorByQuat(&ukf->x[UKF2_STATE_VELX], &ukf->x[UKF2_STATE_Q1], velGlob);

	ukf->pos.x = posGlob[0];
	ukf->pos.y = posGlob[1];
	ukf->pos.z = posGlob[2];

	ukf->vel.x = velGlob[0];
	ukf->vel.y = velGlob[1];
	ukf->vel.z = velGlob[2];

	ukf->accBias.x = ukf->x[UKF2_STATE_ACC_BIAS_X];
	ukf->accBias.y = ukf->x[UKF2_STATE_ACC_BIAS_Y];
	ukf->accBias.z = ukf->x[UKF2_STATE_ACC_BIAS_Z];

	ukf->gyoBias.x = ukf->x[UKF2_STATE_GYO_BIAS_X];
	ukf->gyoBias.y = ukf->x[UKF2_STATE_GYO_BIAS_Y];
	ukf->gyoBias.z = ukf->x[UKF2_STATE_GYO_BIAS_Z];

	memcpy(ukf->q, &ukf->x[UKF2_STATE_Q1], 4*4);
}


