#ifndef NAV_SRCDKF_H_
#define NAV_SRCDKF_H_

#include "srcdkf.h"


#define GRAVITY		9.80665f	// m/s^2



#define SIM_S                   16		// states
#define SIM_M                   3		// max measurements
#define SIM_V                   12		// process noise
#define SIM_N                   3		// max observation noise


#define UKF2_STATE_VELX		0
#define UKF2_STATE_VELY		1
#define UKF2_STATE_VELZ		2
#define UKF2_STATE_POSX		3
#define UKF2_STATE_POSY		4
#define UKF2_STATE_POSZ		5
#define UKF2_STATE_ACC_BIAS_X	6
#define UKF2_STATE_ACC_BIAS_Y	7
#define UKF2_STATE_ACC_BIAS_Z	8
#define UKF2_STATE_GYO_BIAS_X	9
#define UKF2_STATE_GYO_BIAS_Y	10
#define UKF2_STATE_GYO_BIAS_Z	11
#define UKF2_STATE_Q1		12
#define UKF2_STATE_Q2		13
#define UKF2_STATE_Q3		14
#define UKF2_STATE_Q4		15

#define UKF2_V_NOISE_ACC_BIAS_X	0
#define UKF2_V_NOISE_ACC_BIAS_Y	1
#define UKF2_V_NOISE_ACC_BIAS_Z	2
#define UKF2_V_NOISE_GYO_BIAS_X	3
#define UKF2_V_NOISE_GYO_BIAS_Y	4
#define UKF2_V_NOISE_GYO_BIAS_Z	5
#define UKF2_V_NOISE_RATE_X	6
#define UKF2_V_NOISE_RATE_Y	7
#define UKF2_V_NOISE_RATE_Z	8
#define UKF2_V_NOISE_VEL_X	9
#define UKF2_V_NOISE_VEL_Y	10
#define UKF2_V_NOISE_VEL_Z	11

/////////////////////////////////////////////////////////////
#define UKF2_VEL_Q               +3.2545e-03     // +0.032544903471       0.000000350530 +0.000037342305
#define UKF2_VEL_ALT_Q           +1.4483e-03     // +0.144827254833       0.000000347510 -0.000055111229
#define UKF2_POS_Q               +7.1562e-02     // +7156.240473309331    0.000000352142 +2.727925965284749
#define UKF2_POS_ALT_Q           +5.3884e-03     // +5388.369673129109    0.000000351319 -6.187843541372100
#define UKF2_ACC_BIAS_Q          +1.3317e-09     // +0.001331748045       0.000000359470 +0.000000039113
#define UKF2_GYO_BIAS_Q          +4.5256e-09     // +0.045255679186       0.000000349060 +0.000045999290
#define UKF2_QUAT_Q              +5.4005e-02     // +0.000540045060       0.000000353882 +0.000000029711


#define UKF2_ACC_BIAS_V          +7.8673e-09     // +0.000000786725       0.000000345847 -0.000000000977
#define UKF2_GYO_BIAS_V          +4.0297e-09     // +0.000000004030       0.000000359017 +0.000000000000
#define UKF2_RATE_V              +1.7538e-08     // +0.000017538388       0.000000358096 +0.000000000397
#define UKF2_VEL_V               +1.43025e-06     // +0.000000286054       0.000000351709 +0.000000000183

#define UKF2_GPS_POS_N           +8.0703e-06     // +0.000008070349       0.000000353490 +0.000000005602
#define UKF2_GPS_POS_M_N         +3.0245e-05     // +0.000030245341       0.000000345021 -0.000000008396
#define UKF2_GPS_ALT_N           +1.1796e-05     // +0.000011795879       0.000000356036 -0.000000010027
#define UKF2_GPS_ALT_M_N         +3.8329e-05     // +0.000038328879       0.000000346581 +0.000000027268
#define UKF2_GPS_VEL_N           +1.7640e-01     // +0.176404763511       0.000000355574 -0.000094105688
#define UKF2_GPS_VEL_M_N         +3.0138e-02     // +0.030138272888       0.000000343584 -0.000002668997
#define UKF2_GPS_VD_N            +4.6379e+00     // +4.637855992835       0.000000358079 +0.000310962082
#define UKF2_GPS_VD_M_N          +1.3127e-02     // +0.013127146795       0.000000347978 -0.000001550944
#define UKF2_ALT_N               +9.5913e-02     // +0.095913477777       0.000000356359 -0.000049781087
#define UKF2_ACC_N               +6.3287e-05     // +0.000063286884       0.000000342761 -0.000000022717
#define UKF2_DIST_N              +9.7373e-03     // +0.009737270392       0.000000356147 +0.000009059372

#define IMU_ACC_STATIC_STD      0.04f
#define IMU_GYO_STATIC_STD      0.003f
typedef struct{
	srcdkf_t srcdkf;

	vector_t pos, vel, accBias, gyoBias;
	float q[4];

	//camera parameters
	float fx, fy, cx, cy, alpha, beta;
	float qCamToImu[4], posCamToImu[3];

	float realMahalBeacon;
	int stepsBack;

	int cntInitQuat;
	int sizeHist;

	float *posN;
	float *posE;
	float *posD;
	float *velN;
	float *velE;
	float *velD;
	float *q0Hist;
	float *q1Hist;
	float *q2Hist;
	float *q3Hist;
	uint32_t *timeus;

	int navHistIndex;

	float *x;

	float accBuf[64], gyoBuf[64];
	int ptrIMUBuf;
	float SDAcc, SDGyo;
	uint8_t initSDBuf;
	uint8_t motion;

}nav_srcdkf_t;

void nav_srcdkfInit(nav_srcdkf_t *ukf, float *states, int hist);

void nav_srcdkfZeroVel(nav_srcdkf_t *ukf);
void nav_srcdkfZeroRate(nav_srcdkf_t *ukf, float rate, int axis);

void nav_srcdkfBeaconUpdateEUCM(nav_srcdkf_t *ukf, vector_t posBeacon, float pixX, float pixY, float noise, float mahalOfsset, float* realMahal, uint32_t time);

void nav_srcdkfInertialUpdate(nav_srcdkf_t *ukf, vector_t acc, vector_t gyo, vector_t y, float dt, uint32_t time);



#endif /* NAV_SRCDKF_H_ */
