#include "matOperations.h"
#include "math.h"

float dotVector(float *va, float *vb)
{
	return va[0] * vb[0] + va[1] * vb[1] + va[2] * vb[2];
}
float lengthVector(float *va)
{
	float f = dotVector(va, va);
	return sqrtf(f);
}
void normalizeVec(float *v, float *vr)
{
    float norm;
    norm = sqrtf(v[0]*v[0] + v[1]*v[1] + v[2]*v[2]);
	norm = (norm < 0.00000001f) ? 0.0000001f : norm;
    vr[0] = v[0] / norm;
    vr[1] = v[1] / norm;
    vr[2] = v[2] / norm;
}

void normalizeQuat(float *q, float *qr)
{
    float norm;

    norm = 1.0f / sqrtf(q[0]*q[0] + q[1]*q[1] + q[2]*q[2] + q[3]*q[3]);
	norm = (norm < 0.00000001f) ? 0.0000001f : norm;
    qr[0] *= norm;
    qr[1] *= norm;
    qr[2] *= norm;
    qr[3] *= norm;
}

void crossVector(float *w, float *r, float *wr)
{
	wr[0] = -w[2] * r[1] + w[1] * r[2];
	wr[1] = w[2] * r[0] - w[0] * r[2];
	wr[2] = -w[1] * r[0] + w[0] * r[1];
}
void quatMultiply(float *q1, float *q2, float *qr)
{
	qr[0] = q1[0] * q2[0] - q1[1] * q2[1] - q1[2] * q2[2] - q1[3] * q2[3];
	qr[1] = q1[0] * q2[1] + q1[1] * q2[0] + q1[2] * q2[3] - q1[3] * q2[2];
	qr[2] = q1[0] * q2[2] - q1[1] * q2[3] + q1[2] * q2[0] + q1[3] * q2[1];
	qr[3] = q1[0] * q2[3] + q1[1] * q2[2] - q1[2] * q2[1] + q1[3] * q2[0];
}
void rotateVectorByQuat(float *v, float *q, float *vr)
{
    float w, x, y, z;

    w = q[0];
    x = q[1];
    y = q[2];
    z = q[3];

    vr[0] = w*w*v[0] + 2.0f*y*w*v[2] - 2.0f*z*w*v[1] + x*x*v[0] + 2.0f*y*x*v[1] + 2.0f*z*x*v[2] - z*z*v[0] - y*y*v[0];
    vr[1] = 2.0f*x*y*v[0] + y*y*v[1] + 2.0f*z*y*v[2] + 2.0f*w*z*v[0] - z*z*v[1] + w*w*v[1] - 2.0f*x*w*v[2] - x*x*v[1];
    vr[2] = 2.0f*x*z*v[0] + 2.0f*y*z*v[1] + z*z*v[2] - 2.0f*w*y*v[0] - y*y*v[2] + 2.0f*w*x*v[1] - x*x*v[2] + w*w*v[2];
}
void rotateVectorByRevQuat(float *v, float *q, float *vr)
{
    float qc[4];

    qc[0] = q[0];
    qc[1] = -q[1];
    qc[2] = -q[2];
    qc[3] = -q[3];

    rotateVectorByQuat(v, qc, vr);
}
void fastIntegrateQuat(float *qIn, float *rate, float *qOut)
{
    float q[4];
    float r[3];

    r[0] = rate[0] * -0.5f;
    r[1] = rate[1] * -0.5f;
    r[2] = rate[2] * -0.5f;

    q[0] = qIn[0];
    q[1] = qIn[1];
    q[2] = qIn[2];
    q[3] = qIn[3];

    // rotate
    qOut[0] =       q[0] + r[0]*q[1] + r[1]*q[2] + r[2]*q[3];
    qOut[1] = -r[0]*q[0] +      q[1] - r[2]*q[2] + r[1]*q[3];
    qOut[2] = -r[1]*q[0] + r[2]*q[1] +      q[2] - r[0]*q[3];
    qOut[3] = -r[2]*q[0] - r[1]*q[1] + r[0]*q[2] +      q[3];
}
void expIntegrateQuat(float *qIn, float *rate, float *qOut)
{
	float qRate[4];
	exp_mapQuat(rate, qRate);
	quatMultiply(qIn, qRate, qOut);
}

void log_mapQuat(float *qIn, float *omega)
{

	// define these compile time constants to avoid std::abs:
	static const float twoPi = 2.0f * M_PI, NearlyOne = 1.0f - 1e-8f,
		NearlyNegativeOne = -1.0f + 1e-8f;

	const float qw = qIn[0];
	// See Quaternion-Logmap.nb in doc for Taylor expansions
	if (qw >= NearlyOne) {
		// Taylor expansion of (angle / s) at 1
		// (2 + 2 * (1-qw) / 3) * q.vec();
		float tmp = (8.0f / 3.0f - 2.0f / 3.0f * qw);
		omega[0] = tmp * qIn[1];
		omega[1] = tmp * qIn[2];
		omega[2] = tmp * qIn[3];
	}
	else if (qw <= NearlyNegativeOne) {
		// Taylor expansion of (angle / s) at -1
		// (-2 - 2 * (1 + qw) / 3) * q.vec();
		float tmp = (-8.0f / 3.0f - 2.0f / 3.0f * qw);
		omega[0] = tmp * qIn[1];
		omega[1] = tmp * qIn[2];
		omega[2] = tmp * qIn[3];
	}
	else {
		// Normal, away from zero case
		float angle = 2.0f * acosf(qw), s = sqrtf(1.0f - qw * qw);
		// Important:  convert to [-pi,pi] to keep error continuous
		if (angle > M_PI)
			angle -= twoPi;
		else if (angle < -M_PI)
			angle += twoPi;
		float tmp = (angle / s);
		omega[0] = tmp * qIn[1];
		omega[1] = tmp * qIn[2];
		omega[2] = tmp * qIn[3];
	}

}
void exp_mapQuat(float *omega, float *q)
{
	float vec[3];
	float theta2 = dotVector(omega, omega);
	if (theta2 > 5.96e-8f) {
		float theta = sqrtf(theta2);
		float ha = 0.5f * theta;
		float tmp = (sinf(ha) / theta);

		vec[0] = tmp * omega[0];
		vec[1] = tmp * omega[1];
		vec[2] = tmp * omega[2];
		q[0] = cosf(ha);
		q[1] = vec[0];
		q[2] = vec[1];
		q[3] = vec[2];
	}
	else {
		// first order approximation sin(theta/2)/theta = 0.5
		vec[0] = 0.5f * omega[0];
		vec[1] = 0.5f * omega[1];
		vec[2] = 0.5f * omega[2];
		q[0] = 1.0f;
		q[1] = vec[0];
		q[2] = vec[1];
		q[3] = vec[2];
	}
}
void box_plusQuat(float *qIn, float *vec, float *qOut)
{
	float qexp[4];

	if ((vec[0] == 0.0f) && (vec[1] == 0.0f) && (vec[2] == 0.0f))
	{
		qOut[0] = qIn[0];
		qOut[1] = qIn[1];
		qOut[2] = qIn[2];
		qOut[3] = qIn[3];
		return;
	}
	exp_mapQuat(vec, qexp);
	quatMultiply(qexp, qIn, qOut);
}

void box_minusQuat(float *q1, float *q2, float *vec)
{
	float q2inv[4], qr[4];
	if ((q1[0] == q2[0]) && (q1[1] == q2[1]) && (q1[2] == q2[2]) && (q1[3] == q2[3]))
	{
		vec[0] = 0.0f;
		vec[1] = 0.0f;
		vec[2] = 0.0f;
		return;
	}
	q2inv[0] = q2[0];
	q2inv[1] = -q2[1];
	q2inv[2] = -q2[2];
	q2inv[3] = -q2[3];
	quatMultiply(q1, q2inv, qr);
	log_mapQuat(qr, vec);
}
void quatToMatrix(float *m, float *q, int normalize) {
	float sqw = q[0] * q[0];
	float sqx = q[1] * q[1];
	float sqy = q[2] * q[2];
	float sqz = q[3] * q[3];
	float tmp1, tmp2;
	float invs;

	// get the invert square length
	if (normalize)
		invs = 1.0f / (sqx + sqy + sqz + sqw);
	else
		invs = 1.0f;

	// rotation matrix is scaled by inverse square length
	m[0 * 3 + 0] = (sqx - sqy - sqz + sqw) * invs;
	m[1 * 3 + 1] = (-sqx + sqy - sqz + sqw) * invs;
	m[2 * 3 + 2] = (-sqx - sqy + sqz + sqw) * invs;

	tmp1 = q[1] * q[2];
	tmp2 = q[3] * q[0];
	m[1 * 3 + 0] = 2.0f * (tmp1 + tmp2) * invs;
	m[0 * 3 + 1] = 2.0f * (tmp1 - tmp2) * invs;

	tmp1 = q[1] * q[3];
	tmp2 = q[2] * q[0];
	m[2 * 3 + 0] = 2.0f * (tmp1 - tmp2) * invs;
	m[0 * 3 + 2] = 2.0f * (tmp1 + tmp2) * invs;

	tmp1 = q[2] * q[3];
	tmp2 = q[1] * q[0];
	m[2 * 3 + 1] = 2.0f * (tmp1 + tmp2) * invs;
	m[1 * 3 + 2] = 2.0f * (tmp1 - tmp2) * invs;
}
void multiply3x3_3x1(float *ab, float *a, float *b)
{
	ab[0] = a[0] * b[0] + a[1] * b[1] + a[2] * b[2];
	ab[1] = a[3] * b[0] + a[4] * b[1] + a[5] * b[2];
	ab[2] = a[6] * b[0] + a[7] * b[1] + a[8] * b[2];
}
void undistortEUCM(float *vec, float px, float py, float fx, float fy, float cx, float cy, float alpha, float beta)
{
	float mx = (px - cx) / fx;
	float my = (py - cy) / fy;
	float r2 = mx * mx + my * my;
	float mz = (1.0f - beta * alpha*alpha*r2) / (alpha*sqrtf(1.0f - (2.0f*alpha - 1.0f)*beta*r2) + (1.0f - alpha));
	float norm = 1.0f / sqrtf(mz*mz + r2);
	vec[0] = mx * norm;
	vec[1] = my * norm;
	vec[2] = mz * norm;
}
void distortEUCM(float *px, float *py, float *vec, float fx, float fy, float cx, float cy, float alpha, float beta)
{
	float d = sqrtf(beta*(vec[0] * vec[0] + vec[1] * vec[1]) + vec[2] * vec[2]);
	float tmp = alpha * d + (1.0f - alpha)*vec[2];
	*px = fx * vec[0] / tmp + cx;
	*py = fy * vec[1] / tmp + cy;
}
