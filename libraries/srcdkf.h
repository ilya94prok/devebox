#ifndef SRCDKF_H_
#define SRCDKF_H_

#include "math.h"
#include "stdlib.h"
#include "kalman_utils.h"



#define SRCDKF_H	(sqrtf(3.0f) * 3.0f)
#define SRCDKF_RM	0.0001f		// Robbins-Monro stochastic term

typedef void SRCDKFTimeUpdate_t(float *x_I, float *noise_I, float *x_O, float *u, float dt, int n);
typedef void SRCDKFMeasurementUpdate_t(float *u, float *x, float *noise_I, float *y);

// define all temporary storage here so that it does not need to be allocated each iteration
typedef struct {
	int S;
	int V;
	int M;		// only used for parameter estimation
	int N;		// only used for parameter estimation
	int L;

	float32_t h;
	float32_t hh;
	float32_t w0m, wim, wic1, wic2;
	float32_t rm;

	arm_matrix_instance_f32 Sx;	// state covariance
	arm_matrix_instance_f32 SxT;	// Sx transposed
	arm_matrix_instance_f32 Sv;	// process noise
	arm_matrix_instance_f32 Sn;	// observation noise
	arm_matrix_instance_f32 x;	// state estimate vector
	arm_matrix_instance_f32 Xa;	// augmented sigma points
	float32_t *xIn;
	float32_t *xNoise;
	float32_t *xOut;
	arm_matrix_instance_f32 qrTempS;
	arm_matrix_instance_f32 Y;	// resultant measurements from sigma points
	arm_matrix_instance_f32 y;	// measurement estimate vector
	arm_matrix_instance_f32 qrTempM;
	arm_matrix_instance_f32 Sy;	// measurement covariance
	arm_matrix_instance_f32 SyT;	// Sy transposed
	arm_matrix_instance_f32 SyC;	// copy of Sy
	arm_matrix_instance_f32 Pxy;
	arm_matrix_instance_f32 C1;
	arm_matrix_instance_f32 C1T;
	arm_matrix_instance_f32 C2;
	arm_matrix_instance_f32 D;
	arm_matrix_instance_f32 K;
	arm_matrix_instance_f32 KT;	// only used for param est
	arm_matrix_instance_f32 inov;	// inovation
	arm_matrix_instance_f32 inovT;// only used for param est
	arm_matrix_instance_f32 xUpdate;
	arm_matrix_instance_f32 qrFinal;
	arm_matrix_instance_f32 rDiag;
	arm_matrix_instance_f32 Q, R, AQ;	// scratch

	SRCDKFTimeUpdate_t *timeUpdate;

} srcdkf_t;


void srcdkfInit(srcdkf_t *f, int s, int m, int v, int n);
float *srcdkfGetState(srcdkf_t *f);
void srcdkfSetVariance(srcdkf_t *f, float32_t *q, float32_t *v, float32_t *n, int nn);
void srcdkfGetVariance(srcdkf_t *f, float32_t *q);
void srcdkfTimeUpdate(srcdkf_t *f, float32_t *u, float32_t dt, SRCDKFTimeUpdate_t *timeUpdate);
void srcdkfMeasurementUpdate(srcdkf_t *f, float32_t *u, float32_t *y, int M, int N, float32_t *noise, SRCDKFMeasurementUpdate_t *measurementUpdate);
void srcdkfFree(srcdkf_t *f);


#endif /* SRCDKF_H_ */
