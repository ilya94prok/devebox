#include "navigationHandler.h"
#include "string.h"

vector_t vecY = {0,1,0};
static void ukf_Task(void const * argument)
{
	navigationHandler_t *nav = (navigationHandler_t*)argument;

	int16_t lengthIMU = 0;

	osDelay(1000);

	//���� ���� ��������� ����� �� ������ � ������, ����� �� �������, � ������ ����������
	osMutexWait(nav->imu->muxMPUBuf, osWaitForever);
	nav->imu->bufRead = nav->imu->bufWrite;
	osMutexRelease(nav->imu->muxMPUBuf);



	uint32_t lastTimeIMU = 0, lastTimeBeacon = 0;
	uint32_t usStart, usStartFull;
	int delayForUpdate = 0;
	while(1)
	{
		osSemaphoreWait(nav->imu->semReadyProc, osWaitForever);
		usStart = getTimeus();
		usStartFull = usStart;

//		//��������� ���
//		//��������� ��������� ����� ��� ���������
//		//TODO: ���������� lengthIMU
//		//TODO: ��������� ����� �� ��� ���� �����, ��������� ������ �� �����������, ����� �� ������ ������ ������������
		lengthIMU = 0;
		osMutexWait(nav->imu->muxMPUBuf, osWaitForever);
		{
//			lengthIMU = nav->imu->bufWrite - nav->imu->bufRead;
//			if (lengthIMU < 0)
//				lengthIMU += SIZE_BUFFER_IMU;
//			if (lengthIMU != 0)
//			{
//				if (nav->imu->bufWrite < nav->imu->bufRead)
//				{
//					int size1 = SIZE_BUFFER_IMU - nav->imu->bufRead;
//					int size2 = lengthIMU - size1;
//					memcpy(&nav->imuBuf[0], &nav->imu->circBuf[nav->imu->bufRead], size1*sizeof(circularIMUBuf_t));
//					if (size2 != 0)
//						memcpy(&nav->imuBuf[size1*sizeof(circularIMUBuf_t)], &nav->imu->circBuf[0], size2*sizeof(circularIMUBuf_t));
//				}
//				else
//				{
//					memcpy(&nav->imuBuf[0], &nav->imu->circBuf[nav->imu->bufRead], lengthIMU*sizeof(circularIMUBuf_t));
//				}
//				nav->imu->bufRead = (nav->imu->bufRead + lengthIMU)%SIZE_BUFFER_IMU;
//			}

			while(nav->imu->bufWrite != nav->imu->bufRead)
			{
				nav->imuBuf[lengthIMU] = nav->imu->circBuf[nav->imu->bufRead];
				nav->imu->bufRead = (nav->imu->bufRead + 1)%SIZE_BUFFER_IMU;
				lengthIMU++;
			}

//			lengthIMU = 1;
//			int16_t read = nav->imu->bufWrite - 1;
//			if (read < 0)
//				read += SIZE_BUFFER_IMU;
//			nav->imuBuf[0] = nav->imu->circBuf[read];
		}
		osMutexRelease(nav->imu->muxMPUBuf);
		nav->latencyCopyImuBuf = getTimeus() - usStart;

		//
		for (int i = 0; i < lengthIMU; i++)
		{
			vector_t acc, gyo;
			acc.x = nav->imuBuf[i].accBuf[0];
			acc.y = nav->imuBuf[i].accBuf[1];
			acc.z = nav->imuBuf[i].accBuf[2];

			gyo.x = nav->imuBuf[i].gyoBuf[0];
			gyo.y = nav->imuBuf[i].gyoBuf[1];
			gyo.z = nav->imuBuf[i].gyoBuf[2];
			uint32_t dtInt = nav->imuBuf[i].timeCaptureRawBuf - lastTimeIMU;
			float dtf = (float)dtInt/1000000.0f;
			nav->freqPredictIMU = 1.0f/dtf;
			//������ ��������� �� ������ ������ ����� �������

			usStart = getTimeus();

			nav_srcdkfInertialUpdate(&nav->ukf, acc, gyo, vecY, dtf, nav->imuBuf[i].timeCaptureRawBuf);

			nav->latencyPredict = getTimeus() - usStart;

			if (nav->ukf.motion == 0)
			{
				usStart = getTimeus();
				for (int i = 0; i < 3; i++)
				{
					switch (i)
					{
					case 0: nav_srcdkfZeroRate(&nav->ukf, gyo.x, 0); break;
					case 1: nav_srcdkfZeroRate(&nav->ukf, gyo.y, 1); break;
					case 2: nav_srcdkfZeroRate(&nav->ukf, gyo.z, 2); break;
					}

				}
				nav_srcdkfZeroVel(&nav->ukf);
				nav->latencyZeroVel = getTimeus() - usStart;

			}


			nav->latencyFromImuToResultEstimate = getTimeus() - nav->imuBuf[i].timeCaptureRawBuf;

		lastTimeIMU = nav->imuBuf[i].timeCaptureRawBuf;
		nav->freqPredictThread = 1000000.0f/(float)(getTimeus() - nav->usLastPredict);
		nav->usLastPredict = usStart;
		}
		if (nav->blobs->flagBlobsPrcessed && delayForUpdate > 300)
		{
			usStart = getTimeus();
			nav->blobs->flagBlobsPrcessed = 0;
			//��������� ������

			vector_t posLed;
			posLed.x = nav->blobs->posGlobLed[0];
			posLed.y = nav->blobs->posGlobLed[1];
			posLed.z = nav->blobs->posGlobLed[2];

			for (int i = 0 ; i < nav->blobs->numberBlobsDynamic; i++)
			{
				nav_srcdkfBeaconUpdateEUCM(&nav->ukf, posLed, nav->blobs->blobsDynamic[i].posX, nav->blobs->blobsDynamic[i].posY,
						0.001f, 100, &nav->ukf.realMahalBeacon, nav->blobs->timeCapture);
			}



			nav->freqUpdateBeacon = 1000000.0f/(float)(nav->blobs->timeCapture - lastTimeBeacon);
			nav->latencyUpdateBeacon = getTimeus() - usStart;
			nav->freqUpdateBeaconThread = 1000000.0f/(float)(getTimeus() - nav->usLastBeacon);
			nav->usLastBeacon = usStart;
			lastTimeBeacon = nav->blobs->timeCapture;
			nav->latencyFromCaptureToResultEstimate = getTimeus() - nav->blobs->timeCapture;
		}


		if (delayForUpdate <= 300)
			delayForUpdate++;

		nav->latencyFullCycle = getTimeus() - usStartFull;

	}
}


void NavigationHandlerInit(navigationHandler_t *nav, float *states, int hist, mpu9250_t *imu, findBlobs_t *blobs)
{
	nav->imu = imu;
	nav->blobs = blobs;

	nav_srcdkfInit(&nav->ukf, states, hist);

	osThreadDef(ukfThread, ukf_Task, osPriorityNormal, 0, 2096);
	nav->theadID = osThreadCreate(osThread(ukfThread), nav);



}

