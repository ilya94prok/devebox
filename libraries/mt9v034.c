#include "mt9v034.h"

static void MT9V034_Task(void const * argument);



static uint16_t MT9V034ReadReg(mt9v034_t *cam, uint8_t reg)
{
	uint16_t recData[1], res;

	//�������
	HAL_I2C_Mem_Read_IT(cam->i2c, cam->i2cDevAddr, reg ,I2C_MEMADD_SIZE_8BIT,  (uint8_t*)recData, 2);
	osSemaphoreWait(cam->semINTRX, osWaitForever);
	res = (recData[0]>>8)|((recData[0]&0x00ff)<<8);
	return res;
}
static void MT9V034WriteReg(mt9v034_t *cam, uint8_t reg, uint16_t val)
{
	uint16_t recData[1];

	recData[0] = (val>>8)|((val&0x00ff)<<8);
	HAL_I2C_Mem_Write_IT(cam->i2c, cam->i2cDevAddr, reg ,I2C_MEMADD_SIZE_8BIT, (uint8_t*)recData, 2);
	osSemaphoreWait(cam->semINTRX, osWaitForever);
}

static void MT9V034AutoExposureOn(mt9v034_t *cam)
{
	uint16_t read, write;

	read = MT9V034ReadReg(cam, 0xAF);
	write = read;
	write |= (1<<0);
	MT9V034WriteReg(cam, 0xAF, write);
}

static void MT9V034AutoExposureOff(mt9v034_t *cam)
{
	uint16_t read, write;

	read = MT9V034ReadReg(cam, 0xAF);
	write = read;
	write &= ~(1<<0);
	MT9V034WriteReg(cam, 0xAF, write);
}
static void MT9V034AutoGainOn(mt9v034_t *cam)
{
	uint16_t read, write;

	read = MT9V034ReadReg(cam, 0xAF);
	write = read;
	write |= (1<<1);
	MT9V034WriteReg(cam, 0xAF, write);
}

static void MT9V034AutoGainOff(mt9v034_t *cam)
{
	uint16_t read, write;

	read = MT9V034ReadReg(cam, 0xAF);
	write = read;
	write &= ~(1<<1);
	MT9V034WriteReg(cam, 0xAF, write);
}
static void MT9V034SetBlackLevelCalibration(mt9v034_t *cam, int8_t val, uint8_t automatic)
{
	uint16_t read, write;
	read = MT9V034ReadReg(cam, 0x47);
	write = read;


	if (automatic == 0)
		write |= (1<<0);
	else
		write &= ~(1<<0);

	MT9V034WriteReg(cam, 0x47, write);
	if (automatic == 0)
		MT9V034WriteReg(cam, 0x48, val);
}
static void MT9V034SetExposure(mt9v034_t *cam, float us)
{
	uint16_t read, write;
	float dt = 1000000.0f/(float)cam->freq;
	float usf = us/dt;
	int row = (int)usf/(752);
	int col = (int)usf&(480);
	//coarse shutter width
	write = row;
	MT9V034WriteReg(cam, 0x0B, write);
	read = MT9V034ReadReg(cam, 0x0B);
	//fine shutter width
	write = col;
	MT9V034WriteReg(cam, 0xD5, write);
	read = MT9V034ReadReg(cam, 0xD5);

}
static void MT9V034SetAnalogGain(mt9v034_t *cam, uint8_t gain)
{
	gain = (gain < 16) ? 16 : (gain > 64) ? 64 : gain;

	MT9V034WriteReg(cam, 0x35, gain);
}
static void MT9V034SetBinning(mt9v034_t *cam, Binning_t binning)
{
	uint16_t read, write;
	read = MT9V034ReadReg(cam, 0x0D);
	write = read;
	cam->binning = binning;
	switch(binning)
	{
	case BINNING_1: write &= ~((1<<0)|(1<<1)|(1<<2)|(1<<3)); break;
	case BINNING_2: write |= ((1<<0)|(1<<2)); break;
	case BINNING_4: write |= ((1<<0)|(1<<1)|(1<<2)|(1<<3)); break;
	}
	MT9V034WriteReg(cam, 0x0D, write);
}
static void MT9V034LowLevelInit(mt9v034_t *cam)
{
	uint16_t read, write;


//	HAL_I2C_IsDeviceReady(cam->i2c, 0x90, 5 , 100);
//	HAL_I2C_IsDeviceReady(cam->i2c, 0xb0, 5 , 100);
//	HAL_I2C_IsDeviceReady(cam->i2c, 0x98, 5 , 100);
//	HAL_I2C_IsDeviceReady(cam->i2c, 0xb8, 5 , 100);

	if (cam->size == camFullSize)
		MT9V034SetBinning(cam, BINNING_1);
	else
		MT9V034SetBinning(cam, BINNING_2);
	//horizon blanking
	write = 94;
	MT9V034WriteReg(cam, 0x05, write);
	read = MT9V034ReadReg(cam, 0x05);
	//vertical blanking
	write = 5;
	MT9V034WriteReg(cam, 0x06, write);
	read = MT9V034ReadReg(cam, 0x06);

//
	MT9V034AutoExposureOff(cam);
	MT9V034AutoGainOff(cam);
//
	MT9V034SetExposure(cam, cam->exposureUs);
//
	MT9V034SetBlackLevelCalibration(cam, 0, 0);
	MT9V034SetAnalogGain(cam, 64);


	//����������� � snapshot �����
	read = MT9V034ReadReg(cam, 0x07);
	write = 0x0398;
	MT9V034WriteReg(cam, 0x07, write);
	read = MT9V034ReadReg(cam, 0x07);




}


void MT9V034_I2C_Callback(mt9v034_t *cam)
{
	portBASE_TYPE taskWoken = pdFALSE;
	xSemaphoreGiveFromISR(cam->semINTRX, &taskWoken);
}
void MT9V034_LineCallback(mt9v034_t *cam)
{
	cam->cntLines = (cam->cntLines + 1)%cam->height;
	if (cam->cntLines == 0)
		cam->cntFramesByLines++;
	cam->timeBetweenLines = getTimeus() - cam->timeSaveLine;
	cam->timeSaveLine = getTimeus();
}
void MT9V034_FrameCallback(mt9v034_t *cam)
{
	cam->frameRate = 1000000.0f/(cam->timeCapture - cam->timeCaptureLast);
	cam->cntFrames++;
	cam->timeDelayRecieveFrame = getTimeus()-cam->timeCapture;
}
void MT9V034_ExposureCapture(mt9v034_t *cam, uint16_t GPIO_Pin)
{

	if (cam->pinInt == GPIO_Pin)
	{
		uint32_t us = getTimeus();
		if (HAL_GPIO_ReadPin(cam->intPort, cam->pinInt) == GPIO_PIN_SET)
		{
			cam->firstFrontExposure = us;
		}
		else
		{

			cam->secondFrontExposure = us;
			cam->timeCaptureLast = cam->timeCapture;
			cam->timeCapture = cam->secondFrontExposure/2 + cam->firstFrontExposure/2;
			cam->exposureReal = (float)(cam->secondFrontExposure - cam->firstFrontExposure);
		}
	}
}
void MT9V034Init(mt9v034_t *cam, DCMI_HandleTypeDef *dcmi, DMA_HandleTypeDef *dma, I2C_HandleTypeDef *i2c, GPIO_TypeDef *intPort, 	uint16_t pinInt, camSize_t size, uint8_t *bufCam)
{
	cam->intPort = intPort;
	cam->pinInt = pinInt;
	cam->size = size;
	cam->bufCam = bufCam;
	cam->dcmi = dcmi;
	cam->dma = dma;
	cam->i2c = i2c;

	if (size == camHalfSize)
	{
		cam->width = 376;
		cam->height = 240;
	}
	else
	{
		cam->width = 752;
		cam->height = 480;
	}

	cam->semINTRX = osSemaphoreCreate(NULL, 1);


	cam->i2cDevAddr = 0x90;//0xb0, 0x98, 0xb8
	cam->freq = 26600000;

	cam->exposureUs = 2000;
	cam->exposureUsLast = cam->exposureUs;

	cam->cntFrames = 0;
	cam->cntFramesByLines = 0;
	cam->cntLines = 0;

	osThreadDef(MT9V034hread, MT9V034_Task, osPriorityAboveNormal, 0, 256);
	cam->theadCam = osThreadCreate(osThread(MT9V034hread), cam);
}
static void MT9V034_Task(void const * argument)
{
	mt9v034_t *cam = (mt9v034_t*)argument;

	osSemaphoreWait(cam->semINTRX, osWaitForever);

	MT9V034LowLevelInit(cam);


	while(1)
	{
		osDelay(100);
		if (cam->exposureUs != cam->exposureUsLast)
		{
			cam->exposureUsLast = cam->exposureUs;
			MT9V034SetExposure(cam, cam->exposureUs);
		}
	}

}
