#include "findBlobs.h"
#include "math.h"
#include "string.h"


static inline uint32_t getSquareDistance(uint16_t col1, uint16_t row1, uint16_t col2, uint16_t row2)
{
	int32_t difX = (int32_t)col2 - (int32_t)col1;
	int32_t difY = (int32_t)row2 - (int32_t)row1;

	return difX * difX + difY * difY;
}
static inline uint32_t getSquareDistanceFromNumber(findBlobs_t *find, uint32_t number1, uint32_t number2)
{
	uint16_t row1, col1, row2, col2;

	row1 = number1 / find->width;
	col1 = number1 % find->width;

	row2 = number2 / find->width;
	col2 = number2 % find->width;

	return getSquareDistance(col1, row1, col2, row2);
}
static void FindBlobsProcLine(findBlobs_t *find, uint8_t *buf, uint16_t currentRow)
{
	uint32_t startus;
	SCB_InvalidateDCache_by_Addr((uint32_t *)buf, find->width);

	startus = getTimeus();

	for (int i = 0; i < find->width; i++)
	{
		if (buf[i] > find->thershold && find->countThersholdPixels < MAX_NUMBER_THERSHOLD_PIXELS)
		{
			find->intensityOfThersholdPixels[find->countThersholdPixels] = buf[i];
			find->numberOfThersholdPixels[find->countThersholdPixels] = i + currentRow * find->width;
			find->countThersholdPixels++;
		}
	}

	find->latencyFindWhitePixus = getTimeus() - startus;
}
static void FindBlobsCalcBlobs(findBlobs_t *find)
{
	uint32_t startFindus = getTimeus();
	//������� ������� �����
	find->numberBlobs = 0;


	uint16_t row, col;

	uint8_t newBlob = 1;
	for (int i = 0; i < find->countThersholdPixels; i++)
	{		
		row = find->numberOfThersholdPixels[i] / find->width;
		col = find->numberOfThersholdPixels[i] % find->width;

		newBlob = 1;


		for (int blob = 0; blob < find->numberBlobs; blob++)
		{
			uint16_t posX = find->blobs[blob].posX / find->blobs[blob].weight;
			uint16_t posY = find->blobs[blob].posY / find->blobs[blob].weight;
			//������� ���������� �� ������ ����������� ������ �� �������� ������ �������
			uint32_t squareDistance = (uint32_t)getSquareDistance(posX, posY, col, row);
			//���� ��� ������ �������� �������, �� ��� ��������� � �������� �����
			if (find->radiusSquareFindBlob > squareDistance)
			{
				find->blobs[blob].posX += col * find->intensityOfThersholdPixels[i];
				find->blobs[blob].posY += row * find->intensityOfThersholdPixels[i];
				find->blobs[blob].weight += find->intensityOfThersholdPixels[i];
				newBlob = 0;
				break;
			}

		}
		//������ ��� ��� ������ ������� �� ��������� �� � ������ �� ��������� ������, ������� ����� ����
		if (newBlob)
		{
			find->blobs[find->numberBlobs].posX = col * find->intensityOfThersholdPixels[i];
			find->blobs[find->numberBlobs].posY = row * find->intensityOfThersholdPixels[i];
			find->blobs[find->numberBlobs].weight = find->intensityOfThersholdPixels[i];
			find->numberBlobs++;
		}
	}
//	for (int blob = 0; blob < find->numberBlobs; blob++)
//	{
//		find->blobs[blob].posX /= find->blobs[blob].weight;
//		find->blobs[blob].posY /= find->blobs[blob].weight;
//
//	}
	for (int blob = 0; blob < MAX_NUMER_BLOBS; blob++)
	{
		if (blob < find->numberBlobs)
		{
			find->blobs[blob].posX /= find->blobs[blob].weight;
			find->blobs[blob].posY /= find->blobs[blob].weight;
			find->blobs[blob].posX *=-1;
			find->blobs[blob].posX += find->width/2;
			find->blobs[blob].posY += -find->height/2;
		}
		else
		{
			find->blobs[blob].posX = 0x0fffffff;
			find->blobs[blob].posY = 0x0fffffff;
		}

	}
	//������� ��������� ����� �������
	find->countThersholdPixels = 0;
	find->latencyFind = getTimeus() - startFindus;
}

static void FindBlobsOnce(findBlobs_t *find, uint8_t *buf)
{
	//������� ��������� ����� �������
	uint32_t startus;
	find->countThersholdPixels = 0;
	startus = getTimeus();

	for (int j = 0; j < find->height; j++)
	{
		SCB_InvalidateDCache_by_Addr((uint32_t *)&buf[find->width*j], find->width);
		for (int i = 0; i < find->width; i++)
		{
			if (buf[i + find->width*j] > find->thershold)
			{
				if (find->countThersholdPixels < MAX_NUMBER_THERSHOLD_PIXELS)
				{
					find->intensityOfThersholdPixels[find->countThersholdPixels] = buf[i + find->width*j];
					find->numberOfThersholdPixels[find->countThersholdPixels] = (uint32_t)(i + find->width*j);
					find->countThersholdPixels++;
				}
				else
				{
					find->flagValidFind = 0;
					return;
				}

			}
		}
	}

	find->latencyFindWhitePixus = getTimeus() - startus;

	//��������� ������ �����
	for (int i = 0; i < find->numberBlobs; i++)
		find->blobsLast[i] = find->blobs[i];
	find->numberBlobsLast = find->numberBlobs;

	//������� ������� �����
	find->numberBlobs = 0;


	uint16_t row, col;

	uint8_t newBlob = 1;
	for (int i = 0; i < find->countThersholdPixels; i++)
	{
		row = (uint16_t)(find->numberOfThersholdPixels[i] / find->width);
		col = (uint16_t)(find->numberOfThersholdPixels[i] % find->width);

		newBlob = 1;


		for (int blob = 0; blob < find->numberBlobs; blob++)
		{
			uint16_t posX = (uint16_t)(find->blobs[blob].posX / find->blobs[blob].weight);
			uint16_t posY = (uint16_t)(find->blobs[blob].posY / find->blobs[blob].weight);
			//������� ���������� �� ������ ����������� ������ �� �������� ������ �������
			uint32_t squareDistance = (uint32_t)getSquareDistance(posX, posY, col, row);
			//���� ��� ������ �������� �������, �� ��� ��������� � �������� �����
			if (find->radiusSquareFindBlob > squareDistance)
			{
				find->blobs[blob].posX += col * find->intensityOfThersholdPixels[i];
				find->blobs[blob].posY += row * find->intensityOfThersholdPixels[i];
				find->blobs[blob].weight += find->intensityOfThersholdPixels[i];
				newBlob = 0;
				break;
			}

		}
		//������ ��� ��� ������ ������� �� ��������� �� � ������ �� ��������� ������, ������� ����� ����
		if (newBlob && find->numberBlobs < MAX_NUMER_BLOBS)
		{
			if ( find->numberBlobs < MAX_NUMER_BLOBS)
			{
				find->blobs[find->numberBlobs].posX = col * find->intensityOfThersholdPixels[i];
				find->blobs[find->numberBlobs].posY = row * find->intensityOfThersholdPixels[i];
				find->blobs[find->numberBlobs].weight = find->intensityOfThersholdPixels[i];
				find->numberBlobs++;
			}
			else
			{
				find->flagValidFind = 0;
				return;
			}
		}
	}
	for (int blob = 0; blob < find->numberBlobs; blob++)
	{
		find->blobs[blob].posX /= find->blobs[blob].weight;
		find->blobs[blob].posY /= find->blobs[blob].weight;

	}
	find->flagValidFind = 1;
	find->latencyFind = getTimeus() - startus;
}
static void FindBlobsDynamic(findBlobs_t *find, int32_t distance)
{

	uint32_t start = getTimeus();
	//������� ����� ������� ����� �� �� �������, ��� � � ������� ���
	find->numberBlobsDynamic = 0;

	for (int i = 0; i < find->numberBlobs; i++)
	{
		uint8_t blobMatch = 0;
		for (int k = 0; k < find->numberBlobsLast; k++)
		{
			if (getSquareDistance(find->blobs[i].posX, find->blobs[i].posY, find->blobsLast[k].posX, find->blobsLast[k].posY) < distance)
			{
				blobMatch = 1;
				break;
			}
		}
		if (blobMatch == 0)
		{
			find->blobsDynamic[find->numberBlobsDynamic] = find->blobs[i];
			find->numberBlobsDynamic++;
		}
	}

	find->latencyFindDynamic = getTimeus() - start;

//	//for debug
//	for (int blob = 0; blob < MAX_NUMER_BLOBS; blob++)
//	{
//		if (blob >= find->numberBlobsDynamic)
//		{
//			find->blobsDynamic[blob].posX = 0x0fffffff;
//			find->blobsDynamic[blob].posY = 0x0fffffff;
//		}
//		else
//		{
//			find->blobsDynamic[blob].posY *=-1;
//			find->blobsDynamic[blob].posX -= find->width/2;
//			find->blobsDynamic[blob].posY += find->height/2;
//		}
//	}
}
static void FindBlobsOnce2(findBlobs_t *find, uint8_t *buf)
{
	//������� ��������� ����� �������
	uint32_t startus;
	uint32_t DWTStart;
	uint32_t startFindus = getTimeus();
	find->countThersholdPixels = 0;
	startus = getTimeus();
	DWTStart = DWT->CYCCNT;

	//������� ������� �����
	find->numberBlobs = 0;

	uint16_t row, col;
	uint8_t newBlob;

	for (int j = 0; j < find->height; j++)
	{
		SCB_InvalidateDCache_by_Addr((uint32_t *)&buf[find->width*j], find->width);
		for (int i = 0; i < find->width; i++)
		{
			if (buf[i + find->width*j] > find->thershold)
			{
				row = j;
				col = i;

				newBlob = 1;

				for (int blob = 0; blob < find->numberBlobs; blob++)
				{
					uint16_t posX = find->blobs[blob].posX / find->blobs[blob].weight;
					uint16_t posY = find->blobs[blob].posY / find->blobs[blob].weight;
					//������� ���������� �� ������ ����������� ������ �� �������� ������ �������
					uint32_t squareDistance = (uint32_t)getSquareDistance(posX, posY, col, row);
					//���� ��� ������ �������� �������, �� ��� ��������� � �������� �����
					if (find->radiusSquareFindBlob > squareDistance)
					{
						find->blobs[blob].posX += col * buf[i + find->width*j];
						find->blobs[blob].posY += row * buf[i + find->width*j];
						find->blobs[blob].weight += buf[i + find->width*j];
						newBlob = 0;
						break;
					}

				}
				//������ ��� ��� ������ ������� �� ��������� �� � ������ �� ��������� ������, ������� ����� ����
				if (newBlob && find->numberBlobs < MAX_NUMER_BLOBS)
				{
					find->blobs[find->numberBlobs].posX = col * buf[i + find->width*j];
					find->blobs[find->numberBlobs].posY = row * buf[i + find->width*j];
					find->blobs[find->numberBlobs].weight = buf[i + find->width*j];
					find->numberBlobs++;
				}
			}
		}
	}

	find->latencyFindWhitePixus = getTimeus() - startus;


	for (int blob = 0; blob < find->numberBlobs; blob++)
	{
		find->blobs[blob].posX /= find->blobs[blob].weight;
		find->blobs[blob].posY /= find->blobs[blob].weight;

	}
//	for (int blob = 0; blob < MAX_NUMER_BLOBS; blob++)
//	{
//		if (blob < find->numberBlobs)
//		{
//			find->blobs[blob].posX /= find->blobs[blob].weight;
//			find->blobs[blob].posY /= find->blobs[blob].weight;
//			find->blobs[blob].posY *=-1;
//			find->blobs[blob].posX += find->width/2;
//			find->blobs[blob].posY += -find->height/2;
//		}
//		else
//		{
//			find->blobs[blob].posX = 0x0fffffff;
//			find->blobs[blob].posY = 0x0fffffff;
//		}
//
//	}

	find->latencyFind = getTimeus() - startFindus;
}
static void findBlobs_Task(void const * argument)
{
	findBlobs_t *find = (findBlobs_t*)argument;

	osSemaphoreWait(find->semImageReady, osWaitForever);

	while(1)
	{
		osSemaphoreWait(find->semImageReady, osWaitForever);
		FindBlobsOnce(find, find->imageBuf);
		FindBlobsDynamic(find, 100);

		find->numberLed = find->NOT_USEnumberLed;
		memcpy(find->posGlobLed, find->NOT_USEpos, 3*4);

		find->flagBlobsPrcessed = 1;
		osSemaphoreRelease(find->semBlobsReady);
	}

}
void FindBlobsInit(findBlobs_t *find, uint8_t thershold, uint16_t radiusSquareFindBlob, uint16_t width, uint16_t height)
{
	find->width = width;
	find->height = height;
	find->thershold = thershold;
	find->countThersholdPixels = 0;
	find->numberBlobs = 0;
	find->radiusSquareFindBlob = radiusSquareFindBlob;

	find->numberBlobs = 0;
	find->numberBlobsLast = 0;
	find->numberBlobsDynamic = 0;

	find->semImageReady = osSemaphoreCreate(NULL, 1);
	find->semBlobsReady = osSemaphoreCreate(NULL, 1);

	find->flagBlobsPrcessed = 0;

	osThreadDef(findBlobsThread, findBlobs_Task, osPriorityAboveNormal, 0, 512);
	find->threadID = osThreadCreate(osThread(findBlobsThread), find);
}
void FindBlobsImageReady(findBlobs_t *find, uint8_t *imageBuf, uint32_t timeCapture, uint8_t numberLed, float *pos)
{
	find->timeCapture = timeCapture;
	find->imageBuf = imageBuf;
	find->NOT_USEnumberLed = numberLed;
	memcpy(find->NOT_USEpos, pos, 3*4);
	osSemaphoreRelease(find->semImageReady);
}
