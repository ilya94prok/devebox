
#ifndef UDP_BRIDGE_H_
#define UDP_BRIDGE_H_
#include "main.h"
#include "cmsis_os.h"
#include "udp.h"
#include "mt9v034.h"
#include "mpu9250.h"
#include "ms5611.h"


typedef enum{
 udpMsgImage,
 udpMsgIMU
}udpMsgData_t;


#define NUMBER_LINES_IN_PACKET	6

typedef void CallbackUDPRecieve(uint8_t *buf, uint16_t length);

typedef struct{
	udpMsgData_t kind;
	uint32_t us;
	uint16_t numberOfImagePacket;
	uint16_t countAllpackets;
	uint16_t width;
	uint16_t height;
	float posLed[3];
	uint8_t numberLed;
}__packed headerMsg_t;

typedef struct{
	struct udp_pcb *upcbImage;
	struct udp_pcb *upcbIMU;
	headerMsg_t header;
	uint8_t buf[752*NUMBER_LINES_IN_PACKET + sizeof(headerMsg_t)];
	uint16_t cnt;

	osThreadId theadId;
	osSemaphoreId semINTRX;

	uint8_t ipAddr1;
	uint8_t ipAddr2;
	uint8_t ipAddr3;
	uint8_t ipAddr4;

	uint16_t portLocalIMU;
	uint16_t portLocalImage;
	uint16_t portRemoteImage;
	uint16_t portRemoteIMU;

	uint8_t flagBusy;
	uint8_t flagImage;
	uint32_t cntErrSend;

	mt9v034_t *cam;

	CallbackUDPRecieve *clbRec;

}udpBridge_t;

int UDPBridgeSendImage(udpBridge_t *udp, mt9v034_t *cam);
int UDPBridgeSendIMU(udpBridge_t *udp, mpu9250_t *mpu, ms5611_t *ms5611);
void UDPBridgeInit(udpBridge_t *udp, uint16_t portLocalImage, uint16_t portLocalIMU, uint16_t portRemoteImage, uint16_t portRemoteIMU , uint8_t destAdd1, uint8_t destAdd2, uint8_t destAdd3, uint8_t destAdd4);
void UDPBridgeRegisterCallback(udpBridge_t *udp, CallbackUDPRecieve *clbRec);
#endif /* UDP_BRIDGE_H_ */
