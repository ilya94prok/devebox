#ifndef _find_blobs_h
#define _find_blobs_h

#include "stdint.h"
#include "main.h"
#include "cmsis_os.h"

#define MAX_NUMER_BLOBS					32
#define MAX_NUMBER_THERSHOLD_PIXELS		4096

typedef struct {
	float posX, posY;
	uint32_t weight;
}blob_t;

typedef struct {
	uint16_t width;
	uint16_t height;

	uint32_t numberOfThersholdPixels[MAX_NUMBER_THERSHOLD_PIXELS];
	uint8_t intensityOfThersholdPixels[MAX_NUMBER_THERSHOLD_PIXELS];
	uint16_t countThersholdPixels;
	uint8_t thershold;

	uint16_t radiusSquareFindBlob;

	blob_t blobs[MAX_NUMER_BLOBS];
	uint16_t numberBlobs;

	blob_t blobsLast[MAX_NUMER_BLOBS];
	uint16_t numberBlobsLast;

	blob_t blobsDynamic[MAX_NUMER_BLOBS];
	uint16_t numberBlobsDynamic;


	uint8_t flagValidFind;


	uint32_t latencyFindWhitePixus;
	uint32_t latencyFind;
	uint32_t latencyFindDynamic;


	osThreadId threadID;
	osSemaphoreId semImageReady;
	uint8_t *imageBuf;

	uint8_t flagBlobsPrcessed;
	osSemaphoreId semBlobsReady;
	uint32_t timeCapture;

	float posGlobLed[3];
	int numberLed;

	uint8_t NOT_USEnumberLed;
	float NOT_USEpos[3];

}findBlobs_t;

void FindBlobsInit(findBlobs_t *find, uint8_t thershold, uint16_t radiusSquareFindBlob, uint16_t width, uint16_t height);
void FindBlobsImageReady(findBlobs_t *find, uint8_t *imageBuf, uint32_t timeCapture, uint8_t numberLed, float *pos);

#endif
