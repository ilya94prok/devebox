
#include "udp_bridge.h"
#include <string.h>
#include <stdlib.h>
#include <stdint.h>

void udp_receive_callback(void *arg, struct udp_pcb *upcb, struct pbuf *p, const ip_addr_t *addr, u16_t port)
{
	udpBridge_t *udp = (udpBridge_t*)arg;

	if (udp->clbRec)
		udp->clbRec(p->payload, p->len);

	pbuf_free(p);

}

static void UDPBridge_Task(void const * argument)
{
	udpBridge_t *udp = (udpBridge_t*)argument;
	ip_addr_t DestIPaddr;
	err_t err;

	udp->upcbImage = udp_new();
	if (udp->upcbImage != NULL)
	{
		IP4_ADDR(&DestIPaddr, udp->ipAddr1, udp->ipAddr2, udp->ipAddr3, udp->ipAddr4);
		udp->upcbImage->local_port = udp->portLocalImage;
		err = udp_connect(udp->upcbImage, &DestIPaddr, udp->portRemoteImage);
		if (err == ERR_OK)
		{
		  udp_recv(udp->upcbImage, udp_receive_callback, udp);
		}
	}

	udp->upcbIMU = udp_new();
	if (udp->upcbIMU != NULL)
	{
		IP4_ADDR(&DestIPaddr, udp->ipAddr1, udp->ipAddr2, udp->ipAddr3, udp->ipAddr4);
		udp->upcbIMU->local_port = udp->portLocalIMU;
		err = udp_connect(udp->upcbIMU, &DestIPaddr, udp->portRemoteIMU);
//		if (err == ERR_OK)
//		{
//		  udp_recv(udp->upcbIMU, udp_receive_callback, udp);
//		}
	}

	struct pbuf *p;

	osSemaphoreWait(udp->semINTRX, osWaitForever);

	while(1)
	{
		osSemaphoreWait(udp->semINTRX, osWaitForever);

		if (udp->flagImage)
		{
			udp->header.countAllpackets = udp->cam->height;
			udp->header.kind = udpMsgImage;
			udp->header.numberOfImagePacket = 0;
			udp->header.us = udp->cam->timeCapture;
			udp->header.width = udp->cam->width;
			udp->header.height = udp->cam->height;
			udp->header.numberLed = udp->cam->numberLed;

			memcpy(udp->header.posLed, udp->cam->posLed, 3*4);
			if (udp->cam->height == 480)
			{
				udp->cnt = sizeof(udp->buf);
			}
			else
			{
				udp->cnt = sizeof(udp->buf) - udp->cam->width*NUMBER_LINES_IN_PACKET;
			}

			for (int i = 0; i < udp->cam->height/NUMBER_LINES_IN_PACKET; i++)
			{

				memcpy(&udp->buf[0], &udp->header, sizeof(headerMsg_t));
				SCB_InvalidateDCache_by_Addr((uint32_t *)&udp->cam->bufCam[udp->cam->width*i*NUMBER_LINES_IN_PACKET], udp->cam->width*NUMBER_LINES_IN_PACKET);
				memcpy(&udp->buf[sizeof(headerMsg_t)], &udp->cam->bufCam[udp->cam->width*i*NUMBER_LINES_IN_PACKET], udp->cam->width*NUMBER_LINES_IN_PACKET);

				p = pbuf_alloc(PBUF_TRANSPORT, udp->cnt, PBUF_POOL);
				if (p != NULL)
				{

					pbuf_take(p, (void *)udp->buf, udp->cnt);
					if (udp_send(udp->upcbImage, p))
					{
						udp->cntErrSend++;
						pbuf_free(p);
						break;
					}
					udp->header.numberOfImagePacket++;

					pbuf_free(p);
				}
			}
			udp->flagBusy = 0;
		}
		else
		{
			p = pbuf_alloc(PBUF_TRANSPORT, udp->cnt, PBUF_POOL);
			if (p != NULL)
			{
				pbuf_take(p, (void *)udp->buf, udp->cnt);
				if (udp_send(udp->upcbIMU, p))
					udp->cntErrSend++;

				pbuf_free(p);
			}
			udp->flagBusy = 0;
		}

	}

}
void UDPBridgeInit(udpBridge_t *udp, uint16_t portLocalImage, uint16_t portLocalIMU, uint16_t portRemoteImage, uint16_t portRemoteIMU, uint8_t destAdd1, uint8_t destAdd2, uint8_t destAdd3, uint8_t destAdd4)
{
	udp->ipAddr1 = destAdd1;
	udp->ipAddr2 = destAdd2;
	udp->ipAddr3 = destAdd3;
	udp->ipAddr4 = destAdd4;


	udp->portLocalIMU = portLocalIMU;
	udp->portLocalImage = portLocalImage;
	udp->portRemoteImage = portRemoteImage;
	udp->portRemoteIMU = portRemoteIMU;

	udp->semINTRX = osSemaphoreCreate(NULL, 1);
	osThreadDef(UDP_BridgeThread, UDPBridge_Task, osPriorityAboveNormal, 0, 512);
	udp->theadId = osThreadCreate(osThread(UDP_BridgeThread), udp);
}
void UDPBridgeRegisterCallback(udpBridge_t *udp, CallbackUDPRecieve *clbRec)
{
	udp->clbRec = clbRec;
}
int UDPBridgeSendIMU(udpBridge_t *udp, mpu9250_t *mpu, ms5611_t *ms5611)
{
	if (!udp->flagBusy)
	{
		udp->flagBusy = 1;
		udp->flagImage = 0;

		int16_t lengthIMU = 0, lengthMS = 0;

		osMutexWait(mpu->muxMPUBuf, osWaitForever);
		{
			lengthIMU = mpu->bufWrite - mpu->bufRead;
			if (lengthIMU < 0)
				lengthIMU += SIZE_BUFFER_IMU;
			if (lengthIMU == 0)
			{
				udp->flagBusy =  0;
				osMutexRelease(mpu->muxMPUBuf);
				return 0;
			}

			udp->header.countAllpackets = lengthIMU;
			udp->header.kind = udpMsgIMU;
			udp->header.numberOfImagePacket = 0;
			udp->header.us = 0;

			if (mpu->bufWrite < mpu->bufRead)
			{
				int size1 = SIZE_BUFFER_IMU - mpu->bufRead;
				int size2 = lengthIMU - size1;
				memcpy(&udp->buf[sizeof(headerMsg_t)], &mpu->circBuf[mpu->bufRead], size1*sizeof(circularIMUBuf_t));
				memcpy(&udp->buf[sizeof(headerMsg_t) + size1*sizeof(circularIMUBuf_t)], &mpu->circBuf[0], size2*sizeof(circularIMUBuf_t));
			}
			else
			{
				memcpy(&udp->buf[sizeof(headerMsg_t)], &mpu->circBuf[mpu->bufRead], lengthIMU*sizeof(circularIMUBuf_t));
			}
			mpu->bufRead = (mpu->bufRead + lengthIMU)%SIZE_BUFFER_IMU;
		}
		osMutexRelease(mpu->muxMPUBuf);

		osMutexWait(ms5611->muxBuf, osWaitForever);
		{
			lengthMS = ms5611->bufWrite - ms5611->bufRead;
			if (lengthMS < 0)
				lengthMS += SIZE_BUFFER_MS5611;
			if (lengthMS != 0)
			{
				udp->header.numberOfImagePacket = lengthMS;
				int ptr = sizeof(headerMsg_t) + lengthIMU*sizeof(circularIMUBuf_t);
				if (ms5611->bufWrite < ms5611->bufRead)
				{
					int size1 = SIZE_BUFFER_MS5611 - ms5611->bufRead;
					int size2 = lengthMS - size1;


					memcpy(&udp->buf[ptr], &ms5611->circBuf[ms5611->bufRead], size1*sizeof(circularMS5611_t));
					memcpy(&udp->buf[ptr + size1*sizeof(circularMS5611_t)], &ms5611->circBuf[0], size2*sizeof(circularMS5611_t));
				}
				else
				{
					memcpy(&udp->buf[ptr], &ms5611->circBuf[ms5611->bufRead], lengthMS*sizeof(circularMS5611_t));
				}
				ms5611->bufRead = (ms5611->bufRead + lengthMS)%SIZE_BUFFER_MS5611;
			}

		}
		osMutexRelease(ms5611->muxBuf);


		memcpy(&udp->buf[0], &udp->header, sizeof(headerMsg_t));
		udp->cnt = sizeof(headerMsg_t) + lengthIMU*sizeof(circularIMUBuf_t) + lengthMS*sizeof(circularMS5611_t);

		osSemaphoreRelease(udp->semINTRX);
		return 0;
	}
	else
		return 1;
}
int UDPBridgeSendImage(udpBridge_t *udp, mt9v034_t *cam)
{
	if (!udp->flagBusy)
	{
		udp->cam = cam;
		udp->flagBusy = 1;
		udp->flagImage = 1;
		osSemaphoreRelease(udp->semINTRX);
		return 0;
	}
	else
		return 1;
}
