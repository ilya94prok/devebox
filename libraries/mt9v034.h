#ifndef MT9V034_H_
#define MT9V034_H_

#include "main.h"
#include "cmsis_os.h"

typedef enum{
	BINNING_1,
	BINNING_2,
	BINNING_4
}Binning_t;

typedef enum{
	camFullSize,
	camHalfSize
}camSize_t;

typedef struct{
	DCMI_HandleTypeDef *dcmi;
	DMA_HandleTypeDef *dma;
	I2C_HandleTypeDef *i2c;
	uint16_t pinInt;
	GPIO_TypeDef *intPort;

	osThreadId theadCam;
	osSemaphoreId semINTRX;

	uint8_t i2cDevAddr;
	uint32_t freq;
	Binning_t binning;
	float exposureUs;
	float exposureUsLast;
	float exposureReal;
	float frameRate;
	float timeDelayRecieveFrame;
	float timeBetweenLines;
	uint8_t *bufCam;
	camSize_t size;
	uint16_t width, height;
	uint32_t cntLines, cntFrames, cntFramesByLines;

	uint32_t timeCapture;
	uint32_t timeCaptureLast;
	uint32_t firstFrontExposure;
	uint32_t secondFrontExposure;
	uint32_t timeSaveLine;

	uint8_t numberLed;
	float posLed[3];

}mt9v034_t;

void MT9V034Init(mt9v034_t *cam, DCMI_HandleTypeDef *dcmi, DMA_HandleTypeDef *dma, I2C_HandleTypeDef *i2c, GPIO_TypeDef *intPort, uint16_t pinInt, camSize_t size, uint8_t *bufCam);

void MT9V034_ExposureCapture(mt9v034_t *cam, uint16_t GPIO_Pin);
void MT9V034_I2C_Callback(mt9v034_t *cam);
void MT9V034_LineCallback(mt9v034_t *cam);
void MT9V034_FrameCallback(mt9v034_t *cam);
#endif /* MT9V034_H_ */
