#ifndef MATOPERATIONS_H_
#define MATOPERATIONS_H_

#include "main.h"


float dotVector(float *va, float *vb);
float lengthVector(float *va);
void normalizeVec(float *v, float *vr);
void normalizeQuat(float *q, float *qr);
void crossVector(float *w, float *r, float *wr);
void quatMultiply(float *q1, float *q2, float *qr);
void rotateVectorByQuat(float *v, float *q, float *vr);
void rotateVectorByRevQuat(float *v, float *q, float *vr);
void fastIntegrateQuat(float *qIn, float *rate, float *qOut);
void expIntegrateQuat(float *qIn, float *rate, float *qOut);
void log_mapQuat(float *qIn, float *omega);
void exp_mapQuat(float *omega, float *q);
void box_plusQuat(float *qIn, float *vec, float *qOut);
void box_minusQuat(float *q1, float *q2, float *vec);
void quatToMatrix(float *m, float *q, int normalize);
void multiply3x3_3x1(float *ab, float *a, float *b);
void undistortEUCM(float *vec, float px, float py, float fx, float fy, float cx, float cy, float alpha, float beta);
void distortEUCM(float *px, float *py, float *vec, float fx, float fy, float cx, float cy, float alpha, float beta);


#endif /* MATOPERATIONS_H_ */
