#ifndef KALMAN_UTILS_H_
#define KALMAN_UTILS_H_

#include "math.h"
#include "stdlib.h"
#include "matOperations.h"


#define MIN(a, b) ((a < b) ? a : b)
#define MAX(a, b) ((a > b) ? a : b)

#define SIZE_WORDS_BUF_KALMAN	12000

extern uint32_t bufMemoryKalman[];

typedef struct {
	float x, y, z;
}vector_t;

typedef enum
{
  ARM_MATH_SUCCESS        =  0,        /**< No error */
  ARM_MATH_ARGUMENT_ERROR = -1,        /**< One or more arguments are incorrect */
  ARM_MATH_LENGTH_ERROR   = -2,        /**< Length of data buffer is incorrect */
  ARM_MATH_SIZE_MISMATCH  = -3,        /**< Size of matrices is not compatible with the operation */
  ARM_MATH_NANINF         = -4,        /**< Not-a-number (NaN) or infinity is generated */
  ARM_MATH_SINGULAR       = -5,        /**< Input matrix is singular and cannot be inverted */
  ARM_MATH_TEST_FAILURE   = -6         /**< Test Failed */
} arm_status;

typedef float float32_t;

typedef struct
{
  uint16_t numRows;     /**< number of rows of the matrix.     */
  uint16_t numCols;     /**< number of columns of the matrix.  */
  float32_t *pData;     /**< points to the data of the matrix. */
} arm_matrix_instance_f32;

void aqFree(void *ptr, size_t count, size_t size);
void *aqDataCalloc(uint16_t count, uint16_t size);


arm_status arm_mat_trans_f32(
  const arm_matrix_instance_f32 * pSrc,
        arm_matrix_instance_f32 * pDst);


arm_status arm_mat_mult_f32(
  const arm_matrix_instance_f32 * pSrcA,
  const arm_matrix_instance_f32 * pSrcB,
        arm_matrix_instance_f32 * pDst);


void arm_mat_init_f32(
  arm_matrix_instance_f32 * S,
  uint16_t nRows,
  uint16_t nColumns,
  float32_t * pData);


void arm_std_f32(
  const float32_t * pSrc,
        uint32_t blockSize,
        float32_t * pResult);


void arm_fill_f32(
  float32_t value,
  float32_t * pDst,
  uint32_t blockSize);


__STATIC_FORCEINLINE arm_status arm_sqrt_f32(
  float32_t in,
  float32_t * pOut)
  {
    if (in >= 0.0f)
    {
#if defined ( __CC_ARM )
  #if defined __TARGET_FPU_VFP
      *pOut = __sqrtf(in);
  #else
      *pOut = sqrtf(in);
  #endif

#elif defined ( __ICCARM__ )
  #if defined __ARMVFP__
      __ASM("VSQRT.F32 %0,%1" : "=t"(*pOut) : "t"(in));
  #else
      *pOut = sqrtf(in);
  #endif

#else
      *pOut = sqrtf(in);
#endif

      return (ARM_MATH_SUCCESS);
    }
    else
    {
      *pOut = 0.0f;
      return (ARM_MATH_ARGUMENT_ERROR);
    }
  };

void matrixInit(arm_matrix_instance_f32 *m, int rows, int cols);
void matrixFree(arm_matrix_instance_f32 *m);
void matrixDiv_f32(arm_matrix_instance_f32 *X, arm_matrix_instance_f32 *A, arm_matrix_instance_f32 *B, arm_matrix_instance_f32 *Q, arm_matrix_instance_f32 *R, arm_matrix_instance_f32 *AQ);
int qrDecompositionT_f32(arm_matrix_instance_f32 *A, arm_matrix_instance_f32 *Q, arm_matrix_instance_f32 *R);

#endif /* KALMAN_UTILS_H_ */
